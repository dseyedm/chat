#ifndef SHARED_H
#define SHARED_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>

#include <rpc/xdr.h>

#define SIZE(a) (sizeof(a)/sizeof(*a))
#define LEN(a) (sizeof(a)/sizeof(*a) - 1)
#define UNUSED(a) (void)(a)
#define ZEROM(a) memset(&(a), 0, sizeof(a))
#define LINE fprintf(stderr, "%s: %d %s\n", __FILE__, __LINE__, __func__); fflush(stderr)

#define MACROSTR_IMPL(a) #a
#define MACROSTR(a) MACROSTR_IMPL(a)
#define CHK(a) do { \
	if((a) == -1) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);
#define CHK0(a) do { \
	if((a) == 0) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);
#define CHKN0(a) do { \
	if((a) != 0) { \
		fprintf(stderr, \
			__FILE__ ":" MACROSTR(__LINE__) "   " #a "   failed: %s\n" \
		, strerror(errno)); \
		exit(1); \
	} \
} while(0);

bool username_valid(const char* str);

enum {
	C_CONN,
	S_CONN_ACK,
	S_DELTA,
	C_DELTA_ACK,
	C_MSG,
	S_MSG_ACK,
	C_DISS,
	S_DISS_ACK,
	S_RECEIVE,
	C_RECEIVE_ACK,
	S_USERS,
	S_USERS_ACK,
	S_FORWARD,
	S_FORWARD_ACK,
};

enum {
	UF_GOOD,
	UF_INVALID_TYPE,
	UF_PARSE,
	UF_USERNAME_TOO_LONG
};

// does NOT include \0
#define MAX_N_CLIENTS 16
#define MAX_LEN_USERNAME 16

#define P_HEADER_SZ (sizeof(uint16_t)*2)
#define P_HEADER uint16_t pkt_type; uint16_t pkt_id

bool get_flat_type_id(char* d, size_t size, uint16_t* out_type, uint16_t* out_id);
void print_bytes(char* d, size_t size);

// note free*: the actual packet may be allocated on the stack. frees only internal structures.
// unflattened structs:

//////////////////////////////////////////////////////////////////////////////
typedef struct _c_conn {
	P_HEADER;
	char* desired_username;
} c_conn_t;
char* flatten_c_conn(const c_conn_t* p, size_t* out_size);
int unflatten_c_conn(char* p, size_t size, c_conn_t* out_unf);
void free_c_conn(c_conn_t* unf);
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
typedef struct _client_user {
	uint32_t cid;
	char* username;
} client_user_t;
enum {
	P_CONN_RESPONSE_CONNECTED,
	P_CONN_RESPONSE_NAME_TAKEN,
	P_CONN_RESPONSE_NAME_INVALID,
	P_CONN_RESPONSE_FULL
};
typedef struct _s_conn_ack {
	P_HEADER;
	uint32_t response; // P_CONN_RESPONSE. if !GOOD && !ALREADY_CONNECTED, rest of data is zerod out.
	uint32_t delta_ms; // minimum time in us the server waits to send a new delta.
	uint32_t cid;
} s_conn_ack_t;
char* flatten_s_conn_ack(const s_conn_ack_t* p, size_t* out_size);
int unflatten_s_conn_ack(char* p, size_t size, s_conn_ack_t* out_unf);
void free_s_conn_ack(s_conn_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
typedef struct _msg {
	uint64_t ts; // gettimeofday() -> tv_sec*1000 + tv_usec/1000
	uint32_t cid;
	char* msg;
} msg_t;
typedef struct _s_delta {
	P_HEADER;
	uint32_t n_users;
	client_user_t* users;
} s_delta_t;
char* flatten_s_delta(const s_delta_t* p, size_t* out_size);
int unflatten_s_delta(char* p, size_t size, s_delta_t* out_unf);
void free_s_delta(s_delta_t* unf);
//////////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////
typedef struct _c_delta_ack {
	P_HEADER;
} c_delta_ack_t;
char* flatten_c_delta_ack(const c_delta_ack_t* p, size_t* out_size);
int unflatten_c_delta_ack(char* p, size_t size, c_delta_ack_t* out_unf);
void free_c_delta_ack(c_delta_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////
typedef struct _c_msg {
	P_HEADER;
	uint32_t to;
	char* msg;
} c_msg_t;
char* flatten_c_msg(const c_msg_t* p, size_t* out_size);
int unflatten_c_msg(char* p, size_t size, c_msg_t* out_unf);
void free_c_msg(c_msg_t* unf);
//////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////
enum {
	P_MSG_ACK_RESPONSE_GOOD,
	P_MSG_ACK_RESPONSE_CLIENT_NF, // client not found
};
typedef struct _s_msg_ack {
	P_HEADER;
	uint32_t response;
} s_msg_ack_t;
char* flatten_s_msg_ack(const s_msg_ack_t* p, size_t* out_size);
int unflatten_s_msg_ack(char* p, size_t size, s_msg_ack_t* out_unf);
void free_s_msg_ack(s_msg_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////
typedef struct _s_receive {
	P_HEADER;
	uint32_t from;
	char* msg;
} s_receive_t;
char* flatten_s_receive(const s_receive_t* p, size_t* out_size);
int unflatten_s_receive(char* p, size_t size, s_receive_t* out_unf);
void free_s_receive(s_receive_t* unf);
//////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////
typedef struct _c_receive_ack {
	P_HEADER;
} c_receive_ack_t;
char* flatten_c_receive_ack(const c_receive_ack_t* p, size_t* out_size);
int unflatten_c_receive_ack(char* p, size_t size, c_receive_ack_t* out_unf);
void free_c_receive_ack(c_receive_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////
typedef struct _c_diss {
	P_HEADER;
} c_diss_t;
char* flatten_c_diss(const c_diss_t* p, size_t* out_size);
int unflatten_c_diss(char* p, size_t size, c_diss_t* out_unf);
void free_c_diss(c_diss_t* unf);
//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////
typedef struct _s_diss_ack {
	P_HEADER;
} s_diss_ack_t;
char* flatten_s_diss_ack(const s_diss_ack_t* p, size_t* out_size);
int unflatten_s_diss_ack(char* p, size_t size, s_diss_ack_t* out_unf);
void free_s_diss_ack(s_diss_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
typedef struct _s_users {
	P_HEADER;
	uint32_t sid; // origin
	uint32_t n_users;
	client_user_t* users;
} s_users_t;
char* flatten_s_users(const s_users_t* p, size_t* out_size);
int unflatten_s_users(char* p, size_t size, s_users_t* out_unf);
void free_s_users(s_users_t* unf);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
typedef struct _s_users_ack {
	P_HEADER;
} s_users_ack_t;
char* flatten_s_users_ack(const s_users_ack_t* p, size_t* out_size);
int unflatten_s_users_ack(char* p, size_t size, s_users_ack_t* out_unf);
void free_s_users_ack(s_users_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
typedef struct _s_forward {
	P_HEADER;
	uint32_t from;
	uint32_t to;
	char* msg;
} s_forward_t;
char* flatten_s_forward(const s_forward_t* p, size_t* out_size);
int unflatten_s_forward(char* p, size_t size, s_forward_t* out_unf);
void free_s_forward(s_forward_t* unf);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
typedef struct _s_forward_ack {
	P_HEADER;
} s_forward_ack_t;
char* flatten_s_forward_ack(const s_forward_ack_t* p, size_t* out_size);
int unflatten_s_forward_ack(char* p, size_t size, s_forward_ack_t* out_unf);
void free_s_forward_ack(s_forward_ack_t* unf);
//////////////////////////////////////////////////////////////////////////////

#endif
