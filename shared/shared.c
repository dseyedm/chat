#include "shared.h"

#include "cmp.h"

bool username_valid(const char* str) {
	if(!str) return false;
	int str_len = strlen(str);
	if(0 >= str_len || str_len > MAX_LEN_USERNAME) return false;
	for(int i = 0; i < str_len; ++i) {
		if(!isalpha(str[i]) && !isdigit(str[i]) && str[i] != '_') return false;
	}
	return true;
}

static const char* empty_str = "";

static bool read_bytes(void* data, size_t sz, FILE* fh) {
	return fread(data, sizeof(uint8_t), sz, fh) == (sz*sizeof(uint8_t));
}

static bool file_reader(cmp_ctx_t* ctx, void* data, size_t limit) {
	return read_bytes(data, limit, (FILE*)ctx->buf);
}

static size_t file_writer(cmp_ctx_t *ctx, const void *data, size_t count) {
	return fwrite(data, sizeof(uint8_t), count, (FILE*)ctx->buf);
}

void print_bytes(char* d, size_t size) {
	printf("%lu: ", size);
	for(size_t i = 0; i < size; ++i) {
		printf("%02x ", (uint8_t)d[i]);
	}
	printf("\n");
}

bool get_flat_type_id(char* d, size_t size, uint16_t* out_type, uint16_t* out_id) {
	FILE* fp = fmemopen(d, size, "r"); assert(fp);
	cmp_ctx_t cmp; cmp_init(&cmp, fp, file_reader, file_writer);

	uint16_t type, id;
	if(!cmp_read_u16(&cmp, &type)) { 
		fclose(fp);
		return false;
	}
	if(!cmp_read_u16(&cmp, &id)) {
		fclose(fp);
		return false;
	}
	if(out_type) *out_type = type;
	if(out_id) *out_id = id;
	fclose(fp);
	return true;
}

// fatal! cannot flatten!
#define CHKCMP(a) if(!(a)) { fprintf(stderr, "%s\n", cmp_strerror(&cmp)); exit(1); }
char* flatten_c_conn(const c_conn_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, C_CONN));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	if(p->desired_username) {
		CHKCMP(cmp_write_str(&cmp, p->desired_username, strlen(p->desired_username)));
	} else {
		CHKCMP(cmp_write_str(&cmp, empty_str, 0));
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_c_conn(char* p, size_t size, c_conn_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_c_conn(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != C_CONN) { 
		fclose(fp); free_c_conn(out_unf);
		return UF_INVALID_TYPE;
	}

	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_c_conn(out_unf);
		return UF_PARSE;
	}
	uint32_t str_len;
	if(!cmp_read_str_size(&cmp, &str_len)) {
		fclose(fp); free_c_conn(out_unf);
		return UF_PARSE;
	}
	if(str_len > MAX_LEN_USERNAME) {
		fclose(fp); free_c_conn(out_unf);
		return UF_USERNAME_TOO_LONG;
	}
	out_unf->desired_username = (char*)malloc(str_len + 1); assert(out_unf->desired_username);
	if(!read_bytes(out_unf->desired_username, str_len, fp)) {
		fclose(fp); free_c_conn(out_unf);
		return UF_PARSE;
	}
	out_unf->desired_username[str_len] = '\0';
	fclose(fp);
	return UF_GOOD;
}
void free_c_conn(c_conn_t* unf) {
	if(unf->desired_username) free(unf->desired_username);
}

char* flatten_s_conn_ack(const s_conn_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_CONN_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->response));
	CHKCMP(cmp_write_u32(&cmp, p->delta_ms));
	CHKCMP(cmp_write_u32(&cmp, p->cid));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_conn_ack(char* p, size_t size, s_conn_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_CONN_ACK) { 
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->response)) {
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->delta_ms)) {
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->cid)) {
		fclose(fp); free_s_conn_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_conn_ack(s_conn_ack_t* unf) { UNUSED(unf); }

char* flatten_s_delta(const s_delta_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_DELTA));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->n_users));
	for(uint32_t i = 0; i < p->n_users; ++i) {
		CHKCMP(cmp_write_u32(&cmp, p->users[i].cid));
		if(p->users[i].username) {
			CHKCMP(cmp_write_str(&cmp, p->users[i].username, strlen(p->users[i].username)));
		} else {
			CHKCMP(cmp_write_str(&cmp, empty_str, 0));
		}
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_delta(char* p, size_t size, s_delta_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_delta(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_DELTA) { 
		fclose(fp); free_s_delta(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_delta(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->n_users)) {
		fclose(fp); free_s_delta(out_unf);
		return UF_PARSE;
	}

	if(out_unf->n_users) {
		out_unf->users = (client_user_t*)malloc(out_unf->n_users*sizeof(*out_unf->users)); assert(out_unf->users);
		for(uint32_t i = 0; i < out_unf->n_users; ++i) {
			if(!cmp_read_u32(&cmp, &out_unf->users[i].cid)) {
				fclose(fp); free_s_delta(out_unf);
				return UF_PARSE;
			}

			uint32_t str_len;
			if(!cmp_read_str_size(&cmp, &str_len)) {
				fclose(fp); free_s_delta(out_unf);
				return UF_PARSE;
			}
			out_unf->users[i].username = (char*)malloc(str_len + 1); assert(out_unf->users[i].username);
			if(!read_bytes(out_unf->users[i].username, str_len, fp)) {
				fclose(fp); free_s_delta(out_unf);
				return UF_PARSE;
			}
			out_unf->users[i].username[str_len] = '\0';
		}
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_delta(s_delta_t* unf) {
	if(unf->users) {
		for(uint32_t i = 0; i < unf->n_users; ++i) {
			if(unf->users[i].username) free(unf->users[i].username);
		}
		free(unf->users);
	}
}

char* flatten_c_delta_ack(const c_delta_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, C_DELTA_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_c_delta_ack(char* p, size_t size, c_delta_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_c_delta_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != C_DELTA_ACK) { 
		fclose(fp); free_c_delta_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_c_delta_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_c_delta_ack(c_delta_ack_t* unf) { UNUSED(unf); }

char* flatten_c_msg(const c_msg_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, C_MSG));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->to));
	if(p->msg) {
		CHKCMP(cmp_write_str(&cmp, p->msg, strlen(p->msg)));
	} else {
		CHKCMP(cmp_write_str(&cmp, empty_str, 0));
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_c_msg(char* p, size_t size, c_msg_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_c_msg(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != C_MSG) { 
		fclose(fp); free_c_msg(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_c_msg(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->to)) {
		fclose(fp); free_c_msg(out_unf);
		return UF_PARSE;
	}

	uint32_t str_len;
	if(!cmp_read_str_size(&cmp, &str_len)) {
		fclose(fp); free_c_msg(out_unf);
		return UF_PARSE;
	}
	out_unf->msg = (char*)malloc(str_len + 1); assert(out_unf->msg);
	if(!read_bytes(out_unf->msg, str_len, fp)) {
		fclose(fp); free_c_msg(out_unf);
		return UF_PARSE;
	}
	out_unf->msg[str_len] = '\0';

	fclose(fp);
	return UF_GOOD;
}
void free_c_msg(c_msg_t* unf) {
	if(unf->msg) {
		free(unf->msg);
	}
}

char* flatten_s_msg_ack(const s_msg_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_MSG_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->response));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_msg_ack(char* p, size_t size, s_msg_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_msg_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_MSG_ACK) { 
		fclose(fp); free_s_msg_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) { 
		fclose(fp); free_s_msg_ack(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->response)) { 
		fclose(fp); free_s_msg_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_msg_ack(s_msg_ack_t* unf) { UNUSED(unf); }


//////////////////////////////////////////////////////////////////////////////
char* flatten_s_receive(const s_receive_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_RECEIVE));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->from));
	if(p->msg) {
		CHKCMP(cmp_write_str(&cmp, p->msg, strlen(p->msg)));
	} else {
		CHKCMP(cmp_write_str(&cmp, empty_str, 0));
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_receive(char* p, size_t size, s_receive_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_receive(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_RECEIVE) { 
		fclose(fp); free_s_receive(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) { 
		fclose(fp); free_s_receive(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->from)) { 
		fclose(fp); free_s_receive(out_unf);
		return UF_PARSE;
	}

	uint32_t str_len;
	if(!cmp_read_str_size(&cmp, &str_len)) {
		fclose(fp); free_s_receive(out_unf);
		return UF_PARSE;
	}
	out_unf->msg = (char*)malloc(str_len + 1); assert(out_unf->msg);
	if(!read_bytes(out_unf->msg, str_len, fp)) {
		fclose(fp); free_s_receive(out_unf);
		return UF_PARSE;
	}
	out_unf->msg[str_len] = '\0';

	fclose(fp);
	return UF_GOOD;
}
void free_s_receive(s_receive_t* unf) { 
	if(unf->msg) free(unf->msg);
}
//////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////
char* flatten_c_receive_ack(const c_receive_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, C_RECEIVE_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_c_receive_ack(char* p, size_t size, c_receive_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_c_receive_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != C_DISS) { 
		fclose(fp); free_c_receive_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) { 
		fclose(fp); free_c_receive_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_c_receive_ack(c_receive_ack_t* unf) { UNUSED(unf); }
//////////////////////////////////////////////////////////////////////////////




char* flatten_c_diss(const c_diss_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, C_DISS));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_c_diss(char* p, size_t size, c_diss_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_c_diss(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != C_DISS) { 
		fclose(fp); free_c_diss(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) { 
		fclose(fp); free_c_diss(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_c_diss(c_diss_t* unf) { UNUSED(unf); }

char* flatten_s_diss_ack(const s_diss_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_DISS_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_diss_ack(char* p, size_t size, s_diss_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_diss_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_DISS_ACK) { 
		fclose(fp); free_s_diss_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) { 
		fclose(fp); free_s_diss_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_diss_ack(s_diss_ack_t* unf) { UNUSED(unf); }

char* flatten_s_users(const s_users_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_USERS));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->sid));
	CHKCMP(cmp_write_u32(&cmp, p->n_users));
	for(uint32_t i = 0; i < p->n_users; ++i) {
		CHKCMP(cmp_write_u32(&cmp, p->users[i].cid));
		if(p->users[i].username) {
			CHKCMP(cmp_write_str(&cmp, p->users[i].username, strlen(p->users[i].username)));
		} else {
			CHKCMP(cmp_write_str(&cmp, empty_str, 0));
		}
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_users(char* p, size_t size, s_users_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_users(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_USERS) { 
		fclose(fp); free_s_users(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_users(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->sid)) {
		fclose(fp); free_s_users(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->n_users)) {
		fclose(fp); free_s_users(out_unf);
		return UF_PARSE;
	}

	if(out_unf->n_users) {
		out_unf->users = (client_user_t*)malloc(out_unf->n_users*sizeof(*out_unf->users)); assert(out_unf->users);
		for(uint32_t i = 0; i < out_unf->n_users; ++i) {
			if(!cmp_read_u32(&cmp, &out_unf->users[i].cid)) {
				fclose(fp); free_s_users(out_unf);
				return UF_PARSE;
			}

			uint32_t str_len;
			if(!cmp_read_str_size(&cmp, &str_len)) {
				fclose(fp); free_s_users(out_unf);
				return UF_PARSE;
			}
			out_unf->users[i].username = (char*)malloc(str_len + 1); assert(out_unf->users[i].username);
			if(!read_bytes(out_unf->users[i].username, str_len, fp)) {
				fclose(fp); free_s_users(out_unf);
				return UF_PARSE;
			}
			out_unf->users[i].username[str_len] = '\0';
		}
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_users(s_users_t* unf) {
	if(unf->users) {
		for(uint32_t i = 0; i < unf->n_users; ++i) {
			if(unf->users[i].username) free(unf->users[i].username);
		}
		free(unf->users);
	}
}



char* flatten_s_users_ack(const s_users_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_USERS_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_users_ack(char* p, size_t size, s_users_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_users_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_USERS_ACK) { 
		fclose(fp); free_s_users_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_users_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_users_ack(s_users_ack_t* unf) { UNUSED(unf); }


char* flatten_s_forward(const s_forward_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_FORWARD));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));
	CHKCMP(cmp_write_u32(&cmp, p->from));
	CHKCMP(cmp_write_u32(&cmp, p->to));
	if(p->msg) {
		CHKCMP(cmp_write_str(&cmp, p->msg, strlen(p->msg)));
	} else {
		CHKCMP(cmp_write_str(&cmp, empty_str, 0));
	}

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_forward(char* p, size_t size, s_forward_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_FORWARD) { 
		fclose(fp); free_s_forward(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->from)) {
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}
	if(!cmp_read_u32(&cmp, &out_unf->to)) {
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}

	uint32_t str_len;
	if(!cmp_read_str_size(&cmp, &str_len)) {
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}
	out_unf->msg = (char*)malloc(str_len + 1); assert(out_unf->msg);
	if(!read_bytes(out_unf->msg, str_len, fp)) {
		fclose(fp); free_s_forward(out_unf);
		return UF_PARSE;
	}
	out_unf->msg[str_len] = '\0';

	fclose(fp);
	return UF_GOOD;
}
void free_s_forward(s_forward_t* unf) { if(unf->msg) free(unf->msg); }


char* flatten_s_forward_ack(const s_forward_ack_t* p, size_t* out_size) {
	cmp_ctx_t cmp;
	char* block; size_t block_sz;
	FILE* fp = open_memstream(&block, &block_sz); assert(fp);

	cmp_init(&cmp, fp, file_reader, file_writer);

	CHKCMP(cmp_write_u16(&cmp, S_FORWARD_ACK));
	CHKCMP(cmp_write_u16(&cmp, p->pkt_id));

	fclose(fp);
	*out_size = block_sz;
	return block;
}
int unflatten_s_forward_ack(char* p, size_t size, s_forward_ack_t* out_unf) {
	ZEROM(*out_unf);
	FILE* fp = fmemopen(p, size, "r"); assert(fp);
	cmp_ctx_t cmp;
	cmp_init(&cmp, fp, file_reader, file_writer);
	if(!cmp_read_u16(&cmp, &out_unf->pkt_type)) { 
		fclose(fp); free_s_forward_ack(out_unf);
		return UF_PARSE;
	}
	if(out_unf->pkt_type != S_FORWARD_ACK) { 
		fclose(fp); free_s_forward_ack(out_unf);
		return UF_INVALID_TYPE;
	}
	if(!cmp_read_u16(&cmp, &out_unf->pkt_id)) {
		fclose(fp); free_s_forward_ack(out_unf);
		return UF_PARSE;
	}

	fclose(fp);
	return UF_GOOD;
}
void free_s_forward_ack(s_forward_ack_t* unf) { UNUSED(unf); }
