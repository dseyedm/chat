#include <ncurses.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>
#include <unistd.h>
#define SIZE(a) (sizeof(a)/sizeof(*a))
#define LEN(a) (sizeof(a)/sizeof(*a) - 1)
#define SZ 6
int whitelist(char ch) {
	return 32 <= (int)ch && (int)ch <= 126;
}
int main() {
	const char str_prefix[] = "'";
	const char str_middle[] = "' says '";
	const char str_postfix[] = "'!";
	const char* data[SZ][2] = { 
		{ "dickweed_420", "heyy whatsup" },
		{ "john_podesta", "yoo" },
		{ "trumpster",    "MAGA baby wikileaks lol" },
		{ "HRC",          "shutup" },
		{ "hyde_sam",     "soros loooooooo ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo oooooooooooooooooooooooooooooooooooooooooooooooo ooooooooooooooooooooooooooo ooooooooooooooooooooooooooool" },
		{ "trumpster",    "yeeeeeee" }
	};

	initscr(); 
	timeout(0);
	int n_rows, n_cols; getmaxyx(stdscr, n_rows, n_cols);
	start_color();
	init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(2, COLOR_WHITE, COLOR_MAGENTA);

	int row_offset = 0;
	char input[1024] = { 0 };
	int len_input = 0;
	while(1) {
		// render
		const char* empty_line = "~\n";
		for(int i = 0; i <= n_rows - 3; ++i) {
			mvprintw(i, 0, empty_line);
		}
		int curr_row = n_rows - 3 + row_offset;
		for(int i = curr_row - row_offset; i > curr_row; --i) {
			mvprintw(i, 0, empty_line);
		}
		for(int rp = SZ - 1; rp >= 0; --rp) {
			if(curr_row < 0) break;
			int len_data_name = strlen(data[rp][0]);
			int len_data_str = strlen(data[rp][1]);

			if(curr_row + 3 > n_rows) {
				int len_flattened = LEN(str_prefix) + len_data_name + LEN(str_middle) + len_data_str + LEN(str_postfix);
				curr_row -= (len_flattened + n_cols - 1)/n_cols;
				continue;
			}

			char flattened[2048] = { 0 };
			int len_flattened = 0;
			strcpy(flattened, str_prefix); len_flattened += LEN(str_prefix);
			strcpy(flattened + len_flattened, data[rp][0]); len_flattened += len_data_name;
			strcpy(flattened + len_flattened, str_middle); len_flattened += LEN(str_middle);
			strcpy(flattened + len_flattened, data[rp][1]); len_flattened += len_data_str;
			strcpy(flattened + len_flattened, str_postfix); len_flattened += LEN(str_postfix);
			int n_lines = (len_flattened + n_cols - 1)/n_cols; // ceil
			if(curr_row < n_rows) {
				mvprintw(curr_row - len_flattened/n_cols, 0, "%s\n", flattened);
			}
			curr_row -= n_lines;
		}

		// bar
		char bar[n_cols + 1]; for(int i = 0; i < n_cols; ++i) bar[i] = ' '; bar[n_cols] = '\0';
		mvprintw(n_rows - 2, 0, "%s\n", bar);

		mvprintw(n_rows - 1, 0, ":\n"); // input

		refresh(); // render

		// update
		int loop = 1;
//		setlocale(LC_CTYPE, "");
//		raw();
		noecho();
		keypad(stdscr, TRUE);
		mvprintw(n_rows - 1, 0, "\r:%s\n", input);
		while(loop) {
			wint_t ch;
			ch = getch();
			if(ch == -1) continue;
			if(ch == 27) { // esc
				goto die;
			} else if(ch == KEY_UP) { // up
				--row_offset; loop = 0;
			} else if(ch == KEY_DOWN) { // down
				++row_offset; loop = 0;
			} else if(ch == '\r' || ch == '\n') { // enter
				// process input here
				len_input = 0;
				input[len_input] = '\0';
				loop = 0;
			} else if(ch == 127) { // backspace
				if(len_input > 0) --len_input;
				input[len_input] = '\0';
				loop = 0;
			}
			if(whitelist(ch)) {
				input[len_input] = ch;
				input[++len_input] = '\0';
			}
			mvprintw(n_rows - 1, 0, "\r:%s\n", input);
		}
		echo();
	}
die:
	endwin();

	return 0;
}
