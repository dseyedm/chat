#include "shared.h"

#include <ncurses.h>
#include <stdarg.h>
#include <wchar.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <time.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

int g_exit = 0;
void signal_handler(int signal) {
	UNUSED(signal);
	g_exit = 1;
}

const char str_prefix[] = "";
const char str_middle[] = ":";
const char str_postfix[] = "";

// outgoing
const uint32_t trans_resend_every_ms = 150; // resend the transmission every X milliseconds.
const uint32_t trans_expire_ms = 5000; // expire the transmission (and timeout the server)

// incoming
const uint32_t received_expire_ms = 10000; // expire after X milliseconds if no packet has been received

typedef struct _client {
	uint32_t cid;
	char username[MAX_LEN_USERNAME + 1];
} client_t;
typedef struct _transmission {
	uint64_t start_ms; // time since transmission initiated.
	uint64_t send_ms; // time since last send.
	size_t packet_size;
	char* packet;
} transmission_t;

FILE* ferr = NULL;
int this_port = 0;
char* server_ip = NULL;
int server_port = 0;
int socket_handle = 0;
struct sockaddr_in addr_this;
struct sockaddr_in addr_server; 
uint32_t this_client_cid = (uint32_t)(-1);
int n_clients = 0;
client_t clients[MAX_N_CLIENTS];
int n_trans = 0;
transmission_t trans[1024]; // up to X pending outgoing transmissions at once.
bool was_connected = false;
bool connected = false;
uint64_t last_received_ms = 0; // never have i ever received a packet from the server.
uint64_t curr_ts_ms = 0;

typedef struct _log {
	char* msg;
} log_t;
int logs_head = 0; // head = next free space
int logs_tail = 0;
log_t logs[1024];

void write_log(const char* fmt, ...) {
	char* x;
	va_list args; va_start(args, fmt);
	int size = vasprintf(&x, fmt, args); assert(size > 0);
	va_end(args);

	logs[logs_head].msg = x;
	++logs_head; logs_head = logs_head%SIZE(logs);
	if(logs_head == logs_tail) { 
		free(logs[logs_tail].msg); 
		++logs_tail; 
		logs_tail = logs_tail%SIZE(logs);
	}
}

int whitelist(char ch) {
	return 32 <= (int)ch && (int)ch <= 126;
}

void add_client(uint32_t cid, const char* username) {
	assert(n_clients + 1 < MAX_N_CLIENTS);
	assert(strlen(username) <= MAX_LEN_USERNAME);
	clients[n_clients].cid = cid;
	strcpy(clients[n_clients].username, username);
	++n_clients;
}
client_t* get_client(uint32_t cid) {
	for(int i = 0; i < n_clients; ++i) {
		if(cid == clients[i].cid) return &clients[i];
	}
	return NULL;
}
void remove_client(uint32_t cid) {
	for(int i = 0; i < n_clients; ++i) {
		if(cid == clients[i].cid) {
			// swap it with the last element.
			memmove(&clients[i], &clients[n_clients - 1], sizeof(clients[i]));
			// decrement the # of clients.
			--n_clients;
			return;
		}
	}
}

char* smush(void* d, size_t* out_size) {
	uint16_t type = *((uint16_t*)(d)); // (unflattened)
	char* flat_ptr = NULL;
	size_t flat_ptr_size = 0;
	switch(type) {
		case C_CONN:        flat_ptr = flatten_c_conn(d, &flat_ptr_size); break;
		case C_DELTA_ACK:   flat_ptr = flatten_c_delta_ack(d, &flat_ptr_size); break;
		case C_MSG:         flat_ptr = flatten_c_msg(d, &flat_ptr_size); break;
		case C_RECEIVE_ACK: flat_ptr = flatten_c_receive_ack(d, &flat_ptr_size); break;
		case C_DISS:        flat_ptr = flatten_c_diss(d, &flat_ptr_size); break;
		default: assert(0);
	}
	*out_size = flat_ptr_size;
	return flat_ptr;
}
void free_smush(void* d) { free(d); }
void smush_and_send(void* d) {
	size_t flat_ptr_size = 0;
	char* flat_ptr = smush(d, &flat_ptr_size);
	// send flat_ptr
	CHK(sendto(
		socket_handle, 
		flat_ptr, 
		flat_ptr_size, 
		0, 
		(struct sockaddr*)&addr_server, 
		sizeof(addr_server)
	));
	free_smush(flat_ptr);
}

// max + 1
// possibility of overflow uint16_t but very, very unlikely
uint16_t unique_pid() {
	uint16_t id = 0;
	for(int i = 0; i < n_trans; ++i) {
		uint16_t pkt_id;
		assert(get_flat_type_id(trans[i].packet, trans[i].packet_size, NULL, &pkt_id));
		if(pkt_id > id) id = pkt_id;
	}
	return id + 1;
}
void push_trans(void* p) {
	transmission_t t;
	t.start_ms = curr_ts_ms;
	t.send_ms = 0;
	t.packet = smush(p, &t.packet_size);
	memcpy(&trans[n_trans], &t, sizeof(t));
	++n_trans;
}
bool pop_trans(uint16_t packet_type, uint16_t packet_id) {
	int i;
	for(i = 0; i < n_trans; ++i) {
		uint16_t pkt_type, pkt_id;
		assert(get_flat_type_id(
			trans[i].packet, 
			trans[i].packet_size, 
			&pkt_type, 
			&pkt_id
		));
		if(pkt_type == packet_type && pkt_id == packet_id) break;
	}
	if(i >= n_trans) return false; // ignore it! the transmission already timed out.
	// alright! the transmission was successfully completed. pop it.
	free_smush(trans[i].packet);
	memmove(&trans[i], &trans[n_trans - 1], sizeof(trans[i]));
	--n_trans;
	return true;
}

int main(int argc, char* argv[]) {
	assert(signal(SIGINT, signal_handler) != SIG_ERR);
	srand(time(NULL));

	if(argc < 4) goto usage;
	this_port = atoi(argv[1]);
	if(!this_port) goto usage;
	server_ip = argv[2];
	server_port = atoi(argv[3]);
	if(!server_port) goto usage;

	{ // open ferr
		char ferr_str[256] = { 0 };
		int i;
		for(i = 0; i < strlen(argv[0]); ++i) ferr_str[i] = argv[0][i];
		ferr_str[i++] = '_';

		int p = this_port; int pn = 0;
		do { ++pn; } while(p/=10);
		i += pn;
		p = this_port;
		do { ferr_str[--i] = '0' + p%10; } while((p/=10) > 0);
		i += pn;
		ferr_str[i++] = '.';
		ferr_str[i++] = 'l';
		ferr_str[i++] = 'o';
		ferr_str[i++] = 'g';
		ferr = fopen(ferr_str, "w"); assert(ferr);
	}

	CHK(socket_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP));
	CHK(fcntl(socket_handle, F_SETFL, fcntl(socket_handle, F_GETFL) | O_NONBLOCK));

	// get around the "Address already in use" error message on bind() if the client crashes on the same port.
	{
		int true_value = 1;
		CHK(setsockopt(socket_handle, SOL_SOCKET, SO_REUSEADDR, &true_value, sizeof(true_value)));
	}

	ZEROM(addr_this);
	addr_this.sin_family = AF_INET;
	addr_this.sin_port = htons(this_port);
	addr_this.sin_addr.s_addr = INADDR_ANY;
	CHK(bind(socket_handle, (struct sockaddr*)&addr_this, sizeof(addr_this)));

	ZEROM(addr_server);
	addr_server.sin_family = AF_INET;
	addr_server.sin_port = htons(server_port);
	CHK0(inet_aton(server_ip, &addr_server.sin_addr));

	char desired_username[MAX_LEN_USERNAME + 1] = { 0 };
	do {
		printf("username: ");
		scanf("%s", desired_username);
	} while(!username_valid(desired_username));

	initscr(); 
	curs_set(0);
	noecho();
	keypad(stdscr, TRUE);
	timeout(0);
	start_color();
	init_pair(2, COLOR_BLACK, COLOR_WHITE);

	int n_rows, n_cols; getmaxyx(stdscr, n_rows, n_cols);
	int len_input = 0;
	char input[1024] = { 0 }; int row_offset = 0;

	char buffer[1024];
	while(!g_exit) {
		// chill, life's good. wait X ms; update transmissions; check for new packets.
		usleep(16*1000);

		// calculate the current time stamp.
		{
			struct timeval tv; gettimeofday(&tv, 0);
			curr_ts_ms = tv.tv_sec*1000 + tv.tv_usec/1000;
		}

		int size_received;
		// check if we received any new packets. loop through each if there are multiple.
		do {
			struct sockaddr_storage addr_incoming;
			socklen_t len_addr_incoming;
			size_received = recvfrom(
				socket_handle, 
				buffer, 
				SIZE(buffer), 
				0, 
				(struct sockaddr*)&addr_incoming, 
				&len_addr_incoming
			);
			if(size_received <= 0) continue; // ignore this packet.

			struct sockaddr_in* v4_addr_incoming = NULL;

			if(addr_incoming.ss_family == AF_INET) {
				v4_addr_incoming = (struct sockaddr_in*)&addr_incoming;
			} else {
				fprintf(ferr, "warning: ignoring non-IPV4 (ss_family = %d) packet.\n", (int)addr_incoming.ss_family);
				write_log(">warning: ignoring non-IPV4 (ss_family = %d) packet.", (int)addr_incoming.ss_family);
				goto next_packet;
			}

			char incoming_ip[INET6_ADDRSTRLEN] = { 0 }; int incoming_port = 0; {
				CHK0(inet_ntop(
					v4_addr_incoming->sin_family,
					&v4_addr_incoming->sin_addr,
					incoming_ip, sizeof(incoming_ip)
				));
				incoming_port = ntohs(v4_addr_incoming->sin_port);
			}

			if(strcmp(incoming_ip, server_ip) != 0 || incoming_port != server_port) {
				fprintf(ferr, "warning: received packet from %s:%d, which is not the server!\n", incoming_ip, incoming_port);
				write_log(">warning: received packet from %s:%d, which is not the server!", incoming_ip, incoming_port);
				goto next_packet;
			}
			last_received_ms = curr_ts_ms;

			// parse the packet header.
			uint16_t packet_type, packet_id;
			if(!get_flat_type_id(buffer, size_received, &packet_type, &packet_id)) continue;

			// attempt to parse and respond to the packet.
			// ignore if it is malformatted.
			switch(packet_type) {
				case S_CONN_ACK: {
					// pop the matching transmission.
					if(pop_trans(C_CONN, packet_id)) {
						// unflatten & check response.
						s_conn_ack_t unf;
						int result = unflatten_s_conn_ack(buffer, size_received, &unf);
						switch(result) {
							case UF_INVALID_TYPE:
							case UF_PARSE: {
								// strange. ignore.
								goto next_packet;
							}
						}
						static const char* reason[] = { "", "name taken", "name invalid", "server full" };
						switch(unf.response) {
							case P_CONN_RESPONSE_CONNECTED: break;
							case P_CONN_RESPONSE_NAME_TAKEN:
							case P_CONN_RESPONSE_NAME_INVALID:
							case P_CONN_RESPONSE_FULL: {
								fprintf(ferr, "connection refused. reason: %s\n", reason[unf.response]);
								write_log(">connection refused. reason: %s", reason[unf.response]);
								free_s_conn_ack(&unf);
								goto next_packet;
							} break;
							default: { 
								fprintf(ferr, "connection refused. reason: unknown\n"); 
								write_log(">connection refused. reason: unknown"); 
								free_s_conn_ack(&unf); 
								goto next_packet; 
							}
						}
						fprintf(ferr, "connected successfully to %s:%d!\n", incoming_ip, incoming_port);
						write_log(">connected successfully to %s:%d!", incoming_ip, incoming_port);
						// todo: use unf.delta_ms?
						UNUSED(unf.delta_ms);
						// set this_client_cid
						this_client_cid = unf.cid;
						free_s_conn_ack(&unf);
						// we're connected!
						connected = true;
					}
					goto next_packet;
				} break;
				case S_DELTA: {
					// unflatten. update state.
					s_delta_t unf;
					int result = unflatten_s_delta(buffer, size_received, &unf);
					switch(result) {
						case UF_GOOD: break;
						default:
						case UF_INVALID_TYPE:
						case UF_PARSE: {
							// strange. ignore.
							goto next_packet;
						} break;
					}
					// compare the server's connected users with our local copy.
					// validate the packet.
					if(unf.n_users >= SIZE(clients)) {
							fprintf(ferr, "warning: server sent too many clients back\n");
							write_log(">warning: server sent too many clients back");
							// skip the packet.
							free_s_delta(&unf);
							goto next_packet;
					}
					for(uint32_t i = 0; i < unf.n_users; ++i) {
						if(!username_valid(unf.users[i].username)) {
							fprintf(ferr, "warning: server sent a username that is invalid.\n");
							write_log(">warning: server sent a username that is invalid.");
							// skip the packet.
							free_s_delta(&unf);
							goto next_packet;
						}
					}
					// mark surviving (still connected) clients. rename them if neccessary.
					bool connected[MAX_N_CLIENTS] = { 0 };
					for(uint32_t i = 0; i < unf.n_users; ++i) {
						client_t* existing = get_client(unf.users[i].cid);
						if(existing) {
							if(strcmp(existing->username, unf.users[i].username) != 0) {
								fprintf(ferr, "%s changed name to %s\n", existing->username, unf.users[i].username);
								write_log(">%s changed name to %s", existing->username, unf.users[i].username);
								strcpy(existing->username, unf.users[i].username);
							}
							connected[existing - clients] = true;
						}
					}
					// remove any client who did not "survive"
					{
						int n_remove_cids = 0;
						uint32_t remove_cids[MAX_N_CLIENTS] = { 0 };
						for(int i = 0; i < n_clients; ++i) {
							if(!connected[i]) {
								remove_cids[n_remove_cids++] = clients[i].cid;
							}
						}
						for(int i = 0; i < n_remove_cids; ++i) {
							client_t* client = get_client(remove_cids[i]); assert(client);
							fprintf(ferr, "%s disconnected.\n", client->username);
							write_log(">%s disconnected.", client->username);
							remove_client(remove_cids[i]);
						}
					}
					// add new clients who didn't exist in the first place.
					for(uint32_t i = 0; i < unf.n_users; ++i) {
						client_t* existing = get_client(unf.users[i].cid);
						if(!existing) {
							fprintf(ferr, "%s%s connected.\n", unf.users[i].username, 
									(unf.users[i].cid == this_client_cid ? " (you)" : "")
							);
							write_log(">%s%s connected.", unf.users[i].username, 
									(unf.users[i].cid == this_client_cid ? " (you)" : "")
							);
							add_client(unf.users[i].cid, unf.users[i].username);
						}
					}
					free_s_delta(&unf);
					// send an ACK back imm.
					{
						c_delta_ack_t pr; ZEROM(pr);
						pr.pkt_type = C_DELTA_ACK;
						pr.pkt_id = packet_id;
						smush_and_send(&pr);
					}
					goto next_packet;
				} break;
				case S_RECEIVE: {
					// we've received a message!
					s_receive_t unf;
					// unflatten.
					int result = unflatten_s_receive(buffer, size_received, &unf);
					switch(result) {
						case UF_GOOD: break;
						case UF_INVALID_TYPE:
						case UF_PARSE: {
							// strange. ignore.
							goto next_packet;
						} break;
					}
					client_t* existing = get_client(unf.from);
					if(!unf.msg || !strlen(unf.msg)) {
						// strange. ignore.
						free_s_receive(&unf);
						goto next_packet;
					}
					if(!existing) {
						fprintf(ferr, "%d: %s\n", unf.from, unf.msg);
						fprintf(ferr, "warning: received message from unknown user %d\n", unf.from);
						write_log("%d: %s", unf.from, unf.msg);
						write_log(">warning: received message from unknown user %d", unf.from);
						// todo: push to a log.
					} else {
						fprintf(ferr, "%s: %s\n", existing->username, unf.msg);
						write_log("%s: %s", existing->username, unf.msg);
						// todo: push to a log.
					}
					// free unflattened
					free_s_receive(&unf);
					// send an ACK asap.
					{
						c_receive_ack_t pr; ZEROM(pr);
						pr.pkt_type = C_RECEIVE_ACK;
						pr.pkt_id = packet_id;
						smush_and_send(&pr);
					}
					goto next_packet;
				} break;
				case S_MSG_ACK: {
					// alright! the transmission was successfully completed. pop it.
					// unflatten and check response.
				 	// save the cid of the msg.
					if(pop_trans(C_MSG, packet_id)) {
						s_msg_ack_t unf;
						int result = unflatten_s_msg_ack(buffer, size_received, &unf);
						switch(result) {
							case UF_GOOD: break;
							case UF_INVALID_TYPE:
							case UF_PARSE: {
								// strange. ignore.
								goto next_packet;
							} break;
						};
						if(unf.response == P_MSG_ACK_RESPONSE_GOOD) {
							// sent! woohoo!
						} else {
							fprintf(ferr, "warning: message failed to send -- recipient not found.\n");
							write_log(">warning: message failed to send -- recipient not found.");
							// todo: remove the recipient. but w/e. this should rarely happen.
						}
						free_s_msg_ack(&unf);
					}
					goto next_packet;
				} break;
				case S_DISS_ACK: {
					// alright! the transmission was successfully completed. pop it.
					if(pop_trans(C_DISS, packet_id)) {
						// we disconnected cleanly.
						connected = false;
						g_exit = true; // i assume we exit now ? :)
					}
					goto next_packet;
				} break;
				default: goto next_packet;
			}
next_packet:
			continue;
		} while(size_received >= 0); // loop until there aren't any more packets to process.

		// perhaps we haven't had an incoming packet in awhile...
		if(connected && curr_ts_ms > last_received_ms + received_expire_ms) {
			fprintf(ferr, "connection with %s:%d time out after %d ms.\n", server_ip, server_port, received_expire_ms);
			write_log(">connection with %s:%d time out after %d ms.", server_ip, server_port, received_expire_ms);
			// clear current transmissions.
			for(int j = 0; j < n_trans; ++j) {
				free_smush(trans[j].packet);
			}
			n_trans = 0;
			// clear current user list.
			n_clients = 0;
			connected = false;
		}

		// update transmissions.
		for(int i = 0; i < n_trans; ++i) {
			// if the transmission has expired.
			if(curr_ts_ms > trans[i].start_ms + trans_expire_ms) {
				fprintf(ferr, "connection with %s:%d time out after %d ms.\n", server_ip, server_port, trans_expire_ms);
				write_log(">connection with %s:%d time out after %d ms.", server_ip, server_port, trans_expire_ms);
				// clear current transmissions.
				for(int j = 0; j < n_trans; ++j) {
					free_smush(trans[j].packet);
				}
				n_trans = 0;
				// clear current user list.
				n_clients = 0;
				connected = false;
				break;
			}
			// transmission must be sent again!
			if(curr_ts_ms > trans[i].send_ms + trans_resend_every_ms) {
				CHK(sendto(
					socket_handle, 
					trans[i].packet, 
					trans[i].packet_size, 
					0, 
					(struct sockaddr*)&addr_server, 
					sizeof(addr_server)
				));
				// reset send_ms.
				trans[i].send_ms = curr_ts_ms;
			}
			continue;
		}

		if(!connected && was_connected) {
			fprintf(ferr, "disconnected from %s:%d.\n", server_ip, server_port);
			write_log(">disconnected from %s:%d.", server_ip, server_port);
		}
		if(!connected) {
			// look for an outgoing C_CONN transmission.
			// if there isn't one, spawn one and spit out a message.
			bool transmitting_conn = false;
			for(int i = 0; i < n_trans; ++i) {
				uint16_t pkt_type;
				assert(get_flat_type_id(
					trans[i].packet, 
					trans[i].packet_size, 
					&pkt_type, 
					NULL
				));
				if(pkt_type == C_CONN) {
					transmitting_conn = true;
					break;
				}
			}
			if(!transmitting_conn) {
				fprintf(ferr, "attempting to connect to %s:%d...\n", server_ip, server_port);
				write_log(">attempting to connect to %s:%d...", server_ip, server_port);

				assert(username_valid(desired_username));

				// push transmission.
				c_conn_t p; ZEROM(p);
				p.pkt_type = C_CONN;
				p.pkt_id = unique_pid();
				p.desired_username = desired_username;
				push_trans(&p);
			}
		}

		// render
		getmaxyx(stdscr, n_rows, n_cols);
		const char* empty_line = "~\n";
		for(int i = 0; i + 1 < n_rows; ++i) {
			mvprintw(i, 0, empty_line);
		}
		int curr_row = n_rows - 3 + row_offset;
		for(int rp = (logs_head + SIZE(logs) - 1)%SIZE(logs); rp != logs_tail; rp = (rp + SIZE(logs) - 1)%SIZE(logs)) {
			if(curr_row < 0) break;
			int len_flattened = strlen(logs[rp].msg);
			if(curr_row + 3 > n_rows) {
				curr_row -= (len_flattened + n_cols - 1)/n_cols;
				continue;
			}

			int n_lines = (len_flattened + n_cols - 1)/n_cols; // ceil
			if(curr_row < n_rows) {
				mvprintw(curr_row - len_flattened/n_cols, 0, "%s\n", logs[rp].msg);
			}
			curr_row -= n_lines;
		}

		char bar[n_cols + 1]; for(int i = 0; i < n_cols; ++i) bar[i] = ' '; bar[n_cols] = '\0';
		attron(COLOR_PAIR(2));
		mvprintw(n_rows - 2, 0, "%s\n", bar);
		attroff(COLOR_PAIR(2));

		// process input
		{
			wint_t ch = getch();
			if(ch != -1) {
				if(ch == KEY_UP) {
					--row_offset;
				} else if(ch == KEY_DOWN) {
					++row_offset;
				} else if(ch == '\r' || ch == '\n') {
					// process input!
					char dest_username[len_input + 1];
					sscanf(input, "%s", dest_username);
					// loop through every client, searching for dest_username
					int i;
					for(i = 0; i < n_clients; ++i) {
						if(strcmp(clients[i].username, dest_username) == 0) break;
					}
					if(i >= n_clients) {
						write_log(">user '%s' not found. connected users:", dest_username);
						for(int i = 0; i < n_clients; ++i) {
							write_log(">%s%s", clients[i].username, clients[i].cid == this_client_cid ? " (you)" : "");
						}
						len_input = 0;
						input[len_input] = '\0';
					} else if(len_input > 1 + strlen(dest_username)) {
						// send it!
						c_msg_t p; ZEROM(p);
						p.pkt_type = C_MSG;
						p.pkt_id = unique_pid();
						p.to = clients[i].cid;
						p.msg = input + strlen(dest_username) + 1/* ignore space */;
						push_trans(&p);
						write_log("@%s: %s", dest_username, p.msg);

						strcpy(input, dest_username);
						len_input = strlen(dest_username);
						input[len_input++] = ' ';
						input[len_input] = '\0';
					}
				} else if(ch == 127 || ch == 263) {
					// process backspace
					if(len_input > 0) --len_input;
					input[len_input] = '\0';
				} else if(whitelist(ch)) {
					// push character
					if(len_input + 1 < LEN(input)) {
						input[len_input] = ch;
						input[++len_input] = '\0';
					}
				}
			}
			mvprintw(n_rows - 1, 0, "\r@%s\n", input);
		}
		refresh();

		was_connected = connected;
	}

	if(connected) {
		// hmm. maybe we got an exit signal and want to exit ASAP.
		// send a courtesy DISS but don't wait for a reply.
		c_diss_t p; ZEROM(p);
		p.pkt_type = C_DISS;
		p.pkt_id = 0; // we don't care what the server will echo back.
		smush_and_send(&p);
	}

	for(int i = logs_tail; i != logs_head; i = (i + 1)%SIZE(logs)) {
		if(logs[i].msg) free(logs[i].msg);
	}

	endwin();

	CHKN0(close(socket_handle));
	fprintf(ferr, "\nexited successfully.\n");
	CHKN0(fclose(ferr));
	printf("\nexited successfully.\n");
	return 0;
usage:
	fprintf(stderr, "usage: %s <my_port> <server_ip> <server_port>\n", argv[0]);
	return 1;
}
