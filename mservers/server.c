#include "shared.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <signal.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

bool server_alone = false;

// outgoing
const uint32_t trans_resend_every_ms = 250; // resend a transmission every X milliseconds.
const uint32_t trans_expire_ms = 5000; // expire a transmission in X milliseconds.
const uint32_t users_send_every_ms = 1000; // send a user packet every X milliseconds.
const uint32_t delta_send_every_ms = 500; // send a delta packet every X milliseconds.

int g_exit = 0;
void signal_handler(int signal) {
	UNUSED(signal);
	g_exit = 1;
}

typedef struct _client {
	uint32_t sid; // if sid == this_sid, v4_addr is valid.
	struct sockaddr_in v4_addr;
	uint32_t cid;
	char username[MAX_LEN_USERNAME + 1];
} client_t;

typedef struct _transmission {
	uint32_t sid; // if -1, cid is valid. else sid specifies this_sid - 1 or this_sid + 1
	uint32_t cid; // who am i sending this to?

	uint64_t start_ms; // time since transmission initiated.
	uint64_t send_ms; // time since last send.
	size_t packet_size;
	char* packet;
} transmission_t;

int n_servers = 0;
uint32_t this_sid = 0; // [0, n_servers)
struct sockaddr_in right_addr; // i + 1
struct sockaddr_in left_addr; // i - 1
int this_port = 0;
int socket_handle = 0;
struct sockaddr_in this_addr; 
int n_clients = 0; client_t clients[MAX_N_CLIENTS]; 
int n_trans = 0; transmission_t trans[1024];
uint64_t last_delta_ms = 0; // never have i ever sent a delta packet before.
uint64_t last_users_ms = 0; // never have i ever sent a user packet before.
uint64_t curr_ts_ms = 0;

// take the max(client_ids) + 1.
uint32_t generate_cid() {
	uint32_t id;
	while(1) {
		id = rand();
		int i;
		for(i = 0; i < n_clients; ++i) {
			if(clients[i].cid == id) break;
		}
		if(i >= n_clients) break; // unique
	}
	return id;
}
client_t* get_client(uint32_t cid) {
	for(int i = 0; i < n_clients; ++i) {
		if(cid == clients[i].cid) return &clients[i];
	}
	return NULL;
}
void remove_client(uint32_t cid) {
	for(int i = 0; i < n_clients; ++i) {
		if(cid == clients[i].cid) {
			// swap it with the last element.
			memmove(&clients[i], &clients[n_clients - 1], sizeof(clients[i]));
			// decrement the # of clients.
			--n_clients;
			return;
		}
	}
}
client_t* add_local_client(const struct sockaddr_in* addr, const char* username) {
	assert(1 + n_clients < SIZE(clients));
	client_t* new_client = &clients[n_clients]; 
	new_client->sid = this_sid;
	memcpy(&new_client->v4_addr, addr, sizeof(new_client->v4_addr));
	new_client->cid = generate_cid();
	int str_len = strlen(username); assert(str_len <= MAX_LEN_USERNAME);
	strcpy(new_client->username, username); new_client->username[str_len] = '\0';
	++n_clients;
	return new_client;
}
client_t* add_nonlocal_client(uint32_t sid, const char* username, uint32_t cid) {
	assert(!server_alone);
	assert(sid != this_sid);
	assert(1 + n_clients < SIZE(clients));
	client_t* new_client = &clients[n_clients];
	new_client->sid = sid;
	new_client->cid = cid;
	int str_len = strlen(username); assert(str_len <= MAX_LEN_USERNAME);
	strcpy(new_client->username, username); new_client->username[str_len] = '\0';
	++n_clients;
	return new_client;
}

char* smush(void* d, size_t* out_size) {
	uint16_t type = *((uint16_t*)(d)); // (unflattened)
	char* flat_ptr = NULL;
	size_t flat_ptr_size = 0;
	switch(type) {
		case S_CONN_ACK: flat_ptr = flatten_s_conn_ack(d, &flat_ptr_size); break;
		case S_DELTA: flat_ptr = flatten_s_delta(d, &flat_ptr_size); break;
		case S_MSG_ACK: flat_ptr = flatten_s_msg_ack(d, &flat_ptr_size); break;
		case S_RECEIVE: flat_ptr = flatten_s_receive(d, &flat_ptr_size); break;
		case S_DISS_ACK: flat_ptr = flatten_s_diss_ack(d, &flat_ptr_size); break;
		case S_USERS: flat_ptr = flatten_s_users(d, &flat_ptr_size); break;
		case S_USERS_ACK: flat_ptr = flatten_s_users_ack(d, &flat_ptr_size); break;
		case S_FORWARD: flat_ptr = flatten_s_forward(d, &flat_ptr_size); break;
		case S_FORWARD_ACK: flat_ptr = flatten_s_forward_ack(d, &flat_ptr_size); break;
		default: assert(0);
	}
	*out_size = flat_ptr_size;
	return flat_ptr;
}
void free_smush(void* d) { free(d); }
void smush_and_send(void* d, struct sockaddr_in* addr) {
	size_t flat_ptr_size = 0;
	char* flat_ptr = smush(d, &flat_ptr_size);
	// send flat_ptr
	CHK(sendto(
		socket_handle, 
		flat_ptr, 
		flat_ptr_size, 
		0, 
		(struct sockaddr*)addr, 
		sizeof(*addr)
	));
	free_smush(flat_ptr);
}

bool pop_client_trans(uint32_t cid, uint16_t packet_type, uint16_t packet_id) {
	int i;
	for(i = 0; i < n_trans; ++i) {
		if(trans[i].sid != (uint32_t)(-1)) continue; // skip transmissions destined for a server
		if(cid != trans[i].cid) continue;
		uint16_t pkt_type, pkt_id;
		assert(get_flat_type_id(
			trans[i].packet, 
			trans[i].packet_size, 
			&pkt_type, 
			&pkt_id
		));
		if(pkt_type == packet_type && pkt_id == packet_id) break;
	}
	if(i >= n_trans) return false; // ignore it! the transmission already timed out.
	// alright! the transmission was successfully completed. pop it.
	free_smush(trans[i].packet);
	memmove(&trans[i], &trans[n_trans - 1], sizeof(trans[i]));
	--n_trans;
	return true;
}
void push_client_trans(void* p, uint32_t cid) {
	assert(1 + n_trans < SIZE(trans));
	transmission_t t;
	t.sid = (uint32_t)(-1); // no server destination.
	t.cid = cid;
	t.start_ms = curr_ts_ms;
	t.send_ms = 0;
	t.packet = smush(p, &t.packet_size);
	memcpy(&trans[n_trans], &t, sizeof(t));
	++n_trans;
}
bool pop_server_trans(uint32_t sid, uint16_t packet_type, uint16_t packet_id) {
	int i;
	for(i = 0; i < n_trans; ++i) {
		if(trans[i].sid == (uint32_t)(-1)) continue; // skip transmissions destined for a client
		if(sid != trans[i].sid) continue;
		uint16_t pkt_type, pkt_id;
		assert(get_flat_type_id(
			trans[i].packet, 
			trans[i].packet_size, 
			&pkt_type, 
			&pkt_id
		));
		if(pkt_type == packet_type && pkt_id == packet_id) break;
	}
	if(i >= n_trans) return false; // ignore it! the transmission already timed out.
	// alright! the transmission was successfully completed. pop it.
	free_smush(trans[i].packet);
	memmove(&trans[i], &trans[n_trans - 1], sizeof(trans[i]));
	--n_trans;
	return true;
}
void push_server_trans(void* p, uint32_t sid) {
	assert(1 + n_trans < SIZE(trans));
	transmission_t t;
	t.sid = sid; // non client destination.
	t.cid = (uint32_t)(-1);
	t.start_ms = curr_ts_ms;
	t.send_ms = 0;
	t.packet = smush(p, &t.packet_size);
	memcpy(&trans[n_trans], &t, sizeof(t));
	++n_trans;
}

// max + 1
uint16_t unique_pid() {
	uint16_t id = 0;
	for(int i = 0; i < n_trans; ++i) {
		uint16_t pkt_id;
		assert(get_flat_type_id(trans[i].packet, trans[i].packet_size, NULL, &pkt_id));
		if(pkt_id > id) id = pkt_id;
	}
	return id + 1;
}

int main(int argc, char* argv[]) {
	srand(time(NULL));
	assert(signal(SIGINT, signal_handler) != SIG_ERR);

	if(argc < 2) goto usage;

	this_port = atoi(argv[1]);
	if(!this_port) goto usage;

	if(argc == 2) {
		server_alone = true;
	}
	if(!server_alone && argc < 8) goto usage;

	CHK((socket_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)));
	CHK(fcntl(socket_handle, F_SETFL, fcntl(socket_handle, F_GETFL) | O_NONBLOCK));

	// get around the "Address already in use" error message on bind() if the client crashes on the same port.
	{
		int true_value = 1;
		CHK(setsockopt(socket_handle, SOL_SOCKET, SO_REUSEADDR, &true_value, sizeof(true_value)));
	}

	ZEROM(this_addr);
	this_addr.sin_family = AF_INET;
	this_addr.sin_port = htons(this_port);
	this_addr.sin_addr.s_addr = INADDR_ANY;
	CHK(bind(socket_handle, (struct sockaddr*)&this_addr, sizeof(this_addr)));

	if(!server_alone) {
		n_servers = atoi(argv[2]);
		if(!n_servers) goto usage;

		ZEROM(left_addr);
		left_addr.sin_family = AF_INET;
		CHK0(inet_aton(argv[3], &left_addr.sin_addr));
		left_addr.sin_port = htons(atoi(argv[4]));

		ZEROM(right_addr);
		right_addr.sin_family = AF_INET;
		CHK0(inet_aton(argv[5], &right_addr.sin_addr));
		right_addr.sin_port = htons(atoi(argv[6]));

		this_sid = atoi(argv[7]);

		printf("server (sid %d) started on port %d. N servers = %d\n", this_sid, this_port, n_servers);
	} else {
		printf("server started on port %d.\n", this_port);
	}

	char buffer[1024];
	while(!g_exit) {
		// chill, life's good. wait 25ms; update transmissions; send a delta; check for new packets.
		usleep(25*1000);

		// calculate the current time stamp.
		{
			struct timeval tv; gettimeofday(&tv, 0);
			curr_ts_ms = tv.tv_sec*1000 + tv.tv_usec/1000;
		}

		int size_received;
		// check if we received any new packets. loop through each if there are multiple.
		do {
			struct sockaddr_storage addr_incoming;
			socklen_t len_addr_incoming;
			size_received = recvfrom(
				socket_handle, 
				buffer, 
				SIZE(buffer), 
				0, 
				(struct sockaddr*)&addr_incoming, 
				&len_addr_incoming
			);
			if(size_received <= 0) continue; // ignore this packet.

			struct sockaddr_in* v4_addr_incoming = NULL;

			if(addr_incoming.ss_family == AF_INET) {
				v4_addr_incoming = (struct sockaddr_in*)&addr_incoming;
			} else {
				fprintf(stderr, "warning: ignoring non-IPV4 (ss_family = %d) packet.\n", (int)addr_incoming.ss_family);
				continue; // ignore this packet.
			}

			char incoming_ip[INET6_ADDRSTRLEN] = { 0 }; int incoming_port = 0; {
				CHK0(inet_ntop(
					v4_addr_incoming->sin_family,
					&v4_addr_incoming->sin_addr,
					incoming_ip, sizeof(incoming_ip)
				));
				incoming_port = ntohs(v4_addr_incoming->sin_port);
			}

			// parse the packet header.
			uint16_t packet_type, packet_id;
			if(!get_flat_type_id(buffer, size_received, &packet_type, &packet_id)) continue;

			// check if the client is already in our database. (ip + port)
			client_t* existing_client = NULL;
			for(int i = 0; i < n_clients; ++i) {
				if(!server_alone && clients[i].sid != this_sid) continue; // wait for them to time out before adding them locally!
				if(
					memcmp(&v4_addr_incoming->sin_addr, &clients[i].v4_addr.sin_addr, sizeof(v4_addr_incoming->sin_addr)) == 0 &&
					memcmp(&v4_addr_incoming->sin_port, &clients[i].v4_addr.sin_port, sizeof(v4_addr_incoming->sin_port)) == 0) {
					existing_client = &clients[i];
					break;
				}
			}

			uint32_t existing_sid = (uint32_t)(-1);
			if(!server_alone && !existing_client) {
				// check if the server is in our database. (ip + port)
				if(memcmp(&v4_addr_incoming->sin_addr, &right_addr.sin_addr, sizeof(v4_addr_incoming->sin_addr)) == 0 &&
				   memcmp(&v4_addr_incoming->sin_port, &right_addr.sin_port, sizeof(v4_addr_incoming->sin_port)) == 0) {
					existing_sid = (this_sid + 1)%n_servers;
				} else if(memcmp(&v4_addr_incoming->sin_addr, &left_addr.sin_addr, sizeof(v4_addr_incoming->sin_addr)) == 0 &&
				          memcmp(&v4_addr_incoming->sin_port, &left_addr.sin_port, sizeof(v4_addr_incoming->sin_port)) == 0) {
					existing_sid = (this_sid + n_servers - 1)%n_servers;
				}
			}

			// attempt to parse and respond to the packet.
			// ignore if it is malformatted.
			switch(packet_type) {
				case C_CONN: {
					c_conn_t unf;
					int result = unflatten_c_conn(buffer, size_received, &unf);
					switch(result) {
						case UF_GOOD: break;
						case UF_USERNAME_TOO_LONG: {
							// send a response packet back imm.
							s_conn_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_CONN_ACK;
							pr.pkt_id = packet_id;
							pr.response = P_CONN_RESPONSE_NAME_INVALID;
							smush_and_send(&pr, v4_addr_incoming);
							free_c_conn(&unf);
							printf("%s:%d CONN denied: requested username too long.\n", incoming_ip, incoming_port);
							goto next_packet;
						} break;
						case UF_INVALID_TYPE: {
						case UF_PARSE:
							printf("%s:%d CONN denied: failed to parse packet.\n", incoming_ip, incoming_port);
							// dont even bother to reply.
							free_c_conn(&unf);
							goto next_packet;
						 } break;
					}

					// if he is new, check if the requested name string is valid.
					if(!username_valid(unf.desired_username)) {
						// send a response packet back imm.
						s_conn_ack_t pr; ZEROM(pr);
						pr.pkt_type = S_CONN_ACK;
						pr.pkt_id = packet_id;
						pr.response = P_CONN_RESPONSE_NAME_INVALID;
						smush_and_send(&pr, v4_addr_incoming);
						free_c_conn(&unf);
						printf("%s:%d CONN denied: requested username invalid.\n", incoming_ip, incoming_port);
						goto next_packet;
					}

					if(!existing_client) {
						// check if the requested name is already taken.
						bool existing_name = false;
						for(int i = 0; i < n_clients; ++i) {
							if(strcmp(unf.desired_username, clients[i].username) == 0) {
								existing_name = true;
								break;
							}
						}
						if(existing_name) {
							// send a response packet back imm.
							s_conn_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_CONN_ACK;
							pr.pkt_id = packet_id;
							pr.response = P_CONN_RESPONSE_NAME_TAKEN;
							smush_and_send(&pr, v4_addr_incoming);
							printf("%s:%d CONN denied: requested username '%s' already taken.\n", incoming_ip, incoming_port, unf.desired_username);
							free_c_conn(&unf);
							goto next_packet;
						}

						// uh oh, we've reached our maximum capacity!
						if(n_clients + 1 >= SIZE(clients)) {
							s_conn_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_CONN_ACK;
							pr.pkt_id = packet_id;
							pr.response = P_CONN_RESPONSE_FULL;
							// (the rest of pr is left as zeroes)
							smush_and_send(&pr, v4_addr_incoming);
							free_c_conn(&unf);
							printf("%s:%d CONN denied: server full of clients.\n", incoming_ip, incoming_port);
							goto next_packet;
						}

						// physically add the client and set the existing_client pointer.
						existing_client = add_local_client(
							v4_addr_incoming,
							unf.desired_username
						);
						printf("%s:%d CONN accepted: new user %s (%d) connected.\n", incoming_ip, incoming_port, unf.desired_username, existing_client->cid);
					} else {
						// check if his name has changed.
						if(strcmp(existing_client->username, unf.desired_username) != 0) {
							printf("%s:%d CONN accepted: existing user %s (%d) changed name to %s.\n", incoming_ip, incoming_port, existing_client->username, existing_client->cid, unf.desired_username);
							strcpy(existing_client->username, unf.desired_username);
						}
					}

					// send a response packet back imm.
					{
						s_conn_ack_t pr; ZEROM(pr);
						pr.pkt_type = S_CONN_ACK;
						pr.pkt_id = packet_id;
						pr.response = P_CONN_RESPONSE_CONNECTED;
						pr.delta_ms = delta_send_every_ms;
						pr.cid = existing_client->cid;
						smush_and_send(&pr, v4_addr_incoming);
					}
					free_c_conn(&unf);
					goto next_packet;
				} break;
				case C_MSG: {
					if(existing_client) {
						// unflatten
						c_msg_t unf;
						int result = unflatten_c_msg(buffer, size_received, &unf);
						switch(result) {
							case UF_GOOD: break;
							case UF_INVALID_TYPE: {
							case UF_PARSE:
								// dont even bother to reply.
								printf("%s:%d MSG denied: failed to parse.\n", incoming_ip, incoming_port);
								free_c_msg(&unf);
								goto next_packet;
							 } break;
						}

						// check if the destination client exists.
						client_t* dest_client = NULL;
						for(int i = 0; i < n_clients; ++i) {
							if(clients[i].cid == unf.to) {
								dest_client = &clients[i];
								break;
							}
						}

						if(!dest_client) {
							// send a response packet back imm.
							s_msg_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_MSG_ACK;
							pr.pkt_id = packet_id;
							pr.response = P_MSG_ACK_RESPONSE_CLIENT_NF;
							smush_and_send(&pr, v4_addr_incoming);
							free_c_msg(&unf);
							printf("%s:%d MSG denied: failed to find destination client %d.\n", incoming_ip, incoming_port, unf.to);
							goto next_packet;
						}

						// send a "good" ACK asap to the sender.
						{
							s_msg_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_MSG_ACK;
							pr.pkt_id = packet_id;
							pr.response = P_MSG_ACK_RESPONSE_GOOD;
							smush_and_send(&pr, v4_addr_incoming);
						}

						// push a new transmission for the destination client.
						if(dest_client->sid == this_sid) { // we host the destination client!
							s_receive_t pr; ZEROM(pr);
							pr.pkt_type = S_RECEIVE;
							pr.pkt_id = unique_pid();
							pr.from = existing_client->cid;
							pr.msg = unf.msg;
							push_client_trans(&pr, dest_client->cid);
						} else if(!server_alone) {
							// begin forwarding the message.
							// todo: send it the fastest way, given the dest_client's sid.
							s_forward_t pr; ZEROM(pr);
							pr.pkt_type = S_FORWARD;
							pr.pkt_id = unique_pid();
							pr.from = existing_client->cid;
							pr.to = dest_client->cid;
							pr.msg = unf.msg;
							push_server_trans(&pr, (this_sid + 1)%n_servers);
						}

						free_c_msg(&unf);
					}
					goto next_packet;
				} break;
				case C_DISS: {
					if(existing_client) {
						// send an ack back imm.
						s_diss_ack_t pr; ZEROM(pr);
						pr.pkt_type = S_DISS_ACK;
						pr.pkt_id = packet_id;
						smush_and_send(&pr, v4_addr_incoming);

						// remove the client based on his address.
						remove_client(existing_client->cid);
					}
					goto next_packet;
				} break;
				case C_RECEIVE_ACK: {
					if(existing_client) {
						// pop the matching transmission.
						pop_client_trans(existing_client->cid, S_RECEIVE, packet_id);
					}
					goto next_packet;
				} break;
				case C_DELTA_ACK: {
					if(existing_client) {
						// pop the matching transmission.
						pop_client_trans(existing_client->cid, S_DELTA, packet_id);
					}
					goto next_packet;
				} break;
				case S_USERS: {
					if(existing_sid != (uint32_t)(-1)) {
						// unflatten. update state. send it to i + 1, unless it is at the end of the line.
						s_users_t unf;
						int result = unflatten_s_users(buffer, size_received, &unf);
						switch(result) {
							case UF_GOOD: break;
							case UF_INVALID_TYPE: {
							case UF_PARSE:
								printf("%s:%d USERS denied: failed to parse packet.\n", incoming_ip, incoming_port);
								// odd!
								goto next_packet;
							} break;
						}
						// validate the packet.
						if(unf.sid == this_sid) {
							printf("%s:%d USERS denied: packet originates from this server.\n", incoming_ip, incoming_port);
							free_s_users(&unf);
							goto next_packet;
						}
						if(unf.n_users >= SIZE(clients)) {
							printf("%s:%d USERS denied: packet contains too many (>= %d) clients.\n", incoming_ip, incoming_port, (int)SIZE(clients));
							free_s_users(&unf);
							goto next_packet;
						}
						for(uint32_t i = 0; i < unf.n_users; ++i) {
							if(!username_valid(unf.users[i].username)) {
								printf("%s:%d USERS denied: server sent an invalid username.\n", incoming_ip, incoming_port);
								free_s_users(&unf);
								goto next_packet;
							}
						}
						// check for collisions.
						while(1) {
							bool restart = false;
							for(uint32_t i = 0; i < unf.n_users; ++i) {
								for(uint32_t j = 0; j < n_clients; ++j) {
									bool collide_cid = ( // different hosts but identical ids.
										 unf.sid != clients[j].sid &&
										 unf.users[i].cid == clients[j].cid
									);
									bool collide_name = ( // different ids but identical usernames
											unf.users[i].cid != clients[j].cid &&
											strcmp(unf.users[i].username, clients[j].username) == 0
									);
									if(collide_cid) {
										printf("%s:%d USERS warning: cid collision between %s and %s for cid %d.\n", incoming_ip, incoming_port, unf.users[i].username, clients[j].username, clients[j].cid);
									}
									if(collide_name) {
										printf("%s:%d USERS warning: name collision between cid %d and %d for username %s.\n", incoming_ip, incoming_port, unf.users[i].cid, clients[j].cid, clients[j].username);
									}
									// if he collided with our user, kick him!
									if((collide_cid || collide_name) && clients[j].sid == this_sid) {
										remove_client(clients[j].cid);
										// since i,j are wrong. loop again!
										j = n_clients;
										i = unf.n_users;
										restart = true;
									}
								}
							}
							if(!restart) break;
						}
						// remember all of the clients this server used to serve according to our last update.
						// this is our "cold" list.
						uint32_t last_cids[MAX_N_CLIENTS];
						int n_last_cids = 0;
						for(int i = 0; i < n_clients; ++i) {
							if(clients[i].sid != unf.sid) continue;
							last_cids[n_last_cids++] = clients[i].cid;
						}
						// mark surviving (still connected) clients. rename them if neccessary.
						bool connected[MAX_N_CLIENTS] = { 0 };
						for(int i = 0; i < n_last_cids; ++i) {
							// check if this "cold" client still exists in the "hot" update.
							uint32_t j;
							for(j = 0; j < unf.n_users; ++j) {
								if(unf.users[j].cid == last_cids[i]) break;
							}
							if(j >= unf.n_users) continue; // this "cold" user did not survive.
							connected[j] = true; // this user did survive!
							client_t* existing = get_client(last_cids[i]); assert(existing);
							// did his name change?
							if(strcmp(unf.users[j].username, existing->username) != 0) {
								printf("%s changed name to %s\n", existing->username, unf.users[j].username);
								strcpy(existing->username, unf.users[j].username);
							}
						}
						// remove any client who did not "survive"
						{
							for(int i = 0; i < n_last_cids; ++i) {
								// this "cold" client doesn't exist in the "hot" uppdate.
								if(!connected[i]) {
									client_t* client = get_client(last_cids[i]); assert(client);
									printf("%s (server %d) disconnected.\n", client->username, unf.sid);
									remove_client(client->cid);
								}
							}
						}
						// add new clients who didn't exist in the first place.
						for(uint32_t i = 0; i < unf.n_users; ++i) {
							client_t* existing = get_client(unf.users[i].cid);
							if(!existing) {
								if(1 + n_clients < SIZE(clients)) {
									printf("%s (server %d) connected.\n", unf.users[i].username, unf.sid);
									add_nonlocal_client(unf.sid, unf.users[i].username, unf.users[i].cid);
								} else {
									printf("%s:%d USERS denied: packet contains too many (>= %d) clients.\n", incoming_ip, incoming_port, (int)SIZE(clients));
									free_s_users(&unf);
									goto next_packet;
								}
							}
						}
						// send an ACK back imm.
						{
							s_users_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_USERS_ACK;
							pr.pkt_id = packet_id;
							smush_and_send(&pr, v4_addr_incoming);
						}
						// forward the user list to this_i + 1
						uint32_t next_sid = (this_sid + 1)%n_servers;
						if(next_sid != unf.sid/* dont send if it's the source server */) {
							// we're making our own transmission, so init the packet id.
							unf.pkt_id = unique_pid();
							push_server_trans(&unf, next_sid);
						}
						free_s_users(&unf);
					}
					goto next_packet;
				} break;
				case S_FORWARD: {
					if(existing_sid != (uint32_t)(-1)) {
						s_forward_t unf;
						int result = unflatten_s_forward(buffer, size_received, &unf);
						switch(result) {
							case UF_GOOD: break;
							case UF_INVALID_TYPE: {
							case UF_PARSE:
								printf("%s:%d S_FORWARD denied: failed to parse packet.\n", incoming_ip, incoming_port);
								// odd!
								goto next_packet;
							} break;
						}
						client_t* source_client = get_client(unf.from);
						if(!source_client) {
							printf("%s:%d S_FORWARD denied: failed to find source client with cid %d.\n", incoming_ip, incoming_port, unf.from);
							free_s_forward(&unf);
							goto next_packet;
						}
						client_t* dest_client = get_client(unf.to);
						if(!dest_client) {
							printf("%s:%d S_FORWARD denied: failed to find destination client with cid %d.\n", incoming_ip, incoming_port, unf.to);
							free_s_forward(&unf);
							goto next_packet;
						}
						if(dest_client->sid == this_sid) {
							// we host the destination client!
							s_receive_t pr; ZEROM(pr);
							pr.pkt_type = S_RECEIVE;
							pr.pkt_id = unique_pid();
							pr.from = source_client->cid;
							pr.msg = unf.msg;
							push_client_trans(&pr, dest_client->cid);
						} else {
							// forward it to the next server, unless that server hosts the source client...
							// begin forwarding the message in the same order.
							s_forward_t pr; ZEROM(pr);
							pr.pkt_type = S_FORWARD;
							pr.pkt_id = unique_pid();
							pr.from = unf.from;
							pr.to = unf.to;
							pr.msg = unf.msg;
							uint32_t next_sid = (this_sid + 1)%n_servers;
							if(next_sid != source_client->sid) push_server_trans(&pr, next_sid);
						}
						// send an ACK
						{
							s_forward_ack_t pr; ZEROM(pr);
							pr.pkt_type = S_FORWARD_ACK;
							pr.pkt_id = packet_id;
							smush_and_send(&pr, v4_addr_incoming);
						}
						free_s_forward(&unf);
					}
					goto next_packet;
				} break;
				case S_USERS_ACK: {
					if(existing_sid != (uint32_t)(-1)) {
						// pop the matching transmission.
						if(pop_server_trans(existing_sid, S_USERS, packet_id)) { }
					}
					goto next_packet;
				} break;
				case S_FORWARD_ACK: {
					if(existing_sid != (uint32_t)(-1)) {
						// pop the matching transmission.
						if(pop_server_trans(existing_sid, S_FORWARD, packet_id)) { }
					}
					goto next_packet;
				} break;
			}
next_packet:
			continue;
		} while(size_received >= 0); // loop until there aren't any more packets to process.

		// update transmissions.
		for(int i = 0; i < n_trans; ++i) {
			// if the transmission has expired.
			if(curr_ts_ms > trans[i].start_ms + trans_expire_ms) {
				if(trans[i].sid == (uint32_t)(-1)) {
					// get the recipient.
					client_t* client = get_client(trans[i].cid);
					// if he still exists
					if(client) {
						// signal that the user with this sockaddr has timed out.
						printf("%s timed out after %d ms.\n", client->username, trans_expire_ms);
						// remove the user.
						remove_client(client->cid);
					}
					goto remove_transmission;
				} else if(!server_alone) {
					printf("the ");
					if(trans[i].sid == (this_sid + 1)%n_servers) {
						printf("right server (%s:%d)", "??", 0);
					} else {
						printf("left server (%s:%d)", "??", 0);
					}
					printf(" timed out after %d ms!!\n", trans_expire_ms);
					g_exit = 1;
					break; // it's over. one of our dedicated links has died.
				}
			}
			// transmission must be sent again!
			if(curr_ts_ms > trans[i].send_ms + trans_resend_every_ms) {
				// reset send_ms.
				trans[i].send_ms = curr_ts_ms;
				if(trans[i].sid == (uint32_t)(-1)) {
					client_t* c = get_client(trans[i].cid);
					// looks like the client has been disconnected or dropped!
					if(!c) goto remove_transmission;
					assert(c->sid == this_sid);
					CHK(sendto(
						socket_handle, 
						trans[i].packet, 
						trans[i].packet_size, 
						0, 
						(struct sockaddr*)&c->v4_addr, 
						sizeof(c->v4_addr)
					));
				} else if(!server_alone) {
					if(trans[i].sid == (this_sid + 1)%n_servers) {
						CHK(sendto(
							socket_handle, 
							trans[i].packet, 
							trans[i].packet_size, 
							0, 
							(struct sockaddr*)&right_addr, 
							sizeof(right_addr)
						));
					} else {
						CHK(sendto(
							socket_handle, 
							trans[i].packet, 
							trans[i].packet_size, 
							0, 
							(struct sockaddr*)&left_addr, 
							sizeof(left_addr)
						));
					}
				}
			}
			continue;
remove_transmission:
			// remove the transmission by swapping it with the last.
			memmove(&trans[i], &trans[n_trans - 1], sizeof(trans[i]));
			--n_trans;
			--i; // (decrement i to process the one that was just swapped)
			continue;
		}

		// it's time to send a new delta!
		if(curr_ts_ms > last_delta_ms + delta_send_every_ms) {
			s_delta_t p; ZEROM(p);
			p.pkt_type = S_DELTA;

			p.n_users = n_clients;
			client_user_t users[n_clients + 1];
			for(int i = 0; i < n_clients; ++i) {
				client_t* c = &clients[i];
				users[i].cid = c->cid;
				users[i].username = c->username;
			}
			p.users = users;

			// spray this delta to every connected client.
			for(int i = 0; i < n_clients; ++i) {
				if(clients[i].sid == this_sid) {
					p.pkt_id = unique_pid();
					push_client_trans(&p, clients[i].cid);
				}
			}

			last_delta_ms = curr_ts_ms;
		}

		// its time to send a new user packet!
		if(!server_alone && curr_ts_ms > last_users_ms + users_send_every_ms) {
			s_users_t p; ZEROM(p);
			p.pkt_type = S_USERS;
			p.sid = this_sid;

			client_user_t users[MAX_N_CLIENTS];
			p.n_users = 0;
			for(int i = 0; i < n_clients; ++i) {
				if(clients[i].sid != this_sid) continue;
				users[p.n_users].cid = clients[i].cid;
				users[p.n_users].username = clients[i].username;
				++p.n_users;
			}
			p.users = users;

			// send this packet to the server on the right.
			// eventually it will make its way around to the left server, too.
			p.pkt_id = unique_pid();
			push_server_trans(&p, (this_sid + 1)%n_servers);

			last_users_ms = curr_ts_ms;
		}
	}

	CHKN0(close(socket_handle));
	printf("\nexited successfully.\n");
	return 0;

usage:
	fprintf(stderr, "usage: %s <port> <n_servers> <left_ip> <left_port> <right_ip> <right_port> <this_sid>\n", argv[0]);
	return 1;
}
